function $(a) { return document.getElementById(a); }

;(function() {

  // Attendo il caricamento della pagina
  window.addEventListener('load', function() {

  	var porta = (location.port == '') ? '' : location.port

    // Creo una connessione websocket
    var ws = new WebSocket('ws://'+location.hostname+':'+porta)

    // Invio un messaggio di prova al server
    ws.onopen = function() {
      ws.send('Ping.')
    }

    // Messaggio in arrivo
    ws.onmessage = function(msg) {
      $('putHere').insertAdjacentHTML('beforeend', "<p><span class=\"msg\">[SERVER]</span> "+msg.data+"</p>")
    }

    // Conessione chiusa
    ws.onclose = function(msg) {
      $('putHere').insertAdjacentHTML('beforeend', "<p style=\"color: #f44336; text-style: italic;\">Connessione chiusa, ricarica la pagina.</p>")
    }

    // Errore
    ws.onerror = function(msg) {
      $('putHere').insertAdjacentHTML('beforeend', "<p style=\"color: #f44336; text-style: italic;\">Impossibile stabilire una connessione.</p>")
    }


    // Invio i messaggi dell'utente
    $('form').addEventListener('submit', function(e) {

      // Blocco l'esecuzione del form
      e.preventDefault()

      $('putHere').insertAdjacentHTML('beforeend', "<p><span class=\"msg\">[CLIENT]</span> "+$('testo').value+"</p>")

      // Invio il messaggio
      ws.send($('testo').value)

      // Pulisco il form
      $('testo').value = '';
    })
  })
})()
