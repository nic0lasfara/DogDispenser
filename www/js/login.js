function $$(a) { return document.getElementById(a) }

;(function() {

  // Attendo il caricamento della pagina
  window.addEventListener('load', function() {

    // Invio i messaggi dell'utente
    $$('form').addEventListener('submit', function(e) {

      // Blocco l'esecuzione del form
      e.preventDefault()

      var user = $$('username')
      var pwd = $$('pwd')

      // Controllo i dati
      if(user.value === '')
        alert('Devi inserire un nome utente!')

      else if(pwd.value === '')
        alert('Devi inserire una password!')

      else {

        // Creo una nuova richiesta XHR
        var xhr = new XMLHttpRequest()

        xhr.open('POST', '/ajax/accesso', true)
        xhr.setRequestHeader('Content-Type', 'application/json')
        xhr.send(JSON.stringify({ user: user.value, pwd: pwd.value }))

        xhr.onreadystatechange = function() {

          // Se il codice di stato è 200
          // La chiamata al server è andata a buon fine
          if(xhr.readyState === 4 && xhr.status === 200) {

            // Converto la risposta del server in oggetto JSON
            var res = JSON.parse(xhr.response);

            // Controllo se il server ha risposto con un errore
            if(res.errore === true)
              alert(res.msg);

            else
              location.href = '/'


          // Il server ha risposto con un codice di stato diverso
          // Da 200, avviso l'utente dell'errore
          } else if(xhr.readyState === 4 && xhr.status !== 200)
            alert('Errore nella comunicazione col server!')
        }
      }
    })
  })
})()
