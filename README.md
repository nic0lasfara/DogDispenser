#DogDispenser

## Synopsis

At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)

## Code Example

Show what the library does as concisely as possible, developers should be able to figure out **how** your project solves their problem by looking at the code example. Make sure the API you are showing off is obvious, and that your code is short and concise.

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Installation

Provide code examples and explanations of how to get the project.

## Tests

Describe and show how to run the tests with code examples.

## Contributors
+ Nicoals Farabegoli  
+ Andrea Giulianini
+ Fabrizio Margotta


## License

A short snippet describing the license (MIT, Apache, etc.)
