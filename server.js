// Importo le librerie!
var http = require('http')
var express = require('express')
var Ws = require('ws').Server
var fs = require('node-fs-extra')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')

const USERNAME_DEFAULT = 'prova'
const PASSWORD_DEFAULT = 'prova'

// Se il file degli utenti non esiste
// O non è valido lo creo nuovo
try {
    var stat = fs.statSync(__dirname+'/utenti.json')

    if(!stat.isFile())
        creaFile()

    var utenti = fs.readJsonSync(__dirname+'/utenti.json')

    if(utenti.length === 0)
        creaFile()

} catch(e) {
    creaFile();
}
function creaFile() {

    // Creo un utente generico di default
    var utenti = [
        {
          username: USERNAME_DEFAULT
          ,pwd: require('crypto').createHash('md5').update(PASSWORD_DEFAULT).digest('hex') // Hash MD5 della password
        }
    ]

    // Salvo su disco
    ;fs.writeJsonSync(__dirname+'/utenti.json', utenti)
}

// Creo un server HTTP
// Utilizzo il modulo nativo perché così posso far lavorare
// Sia express (che normalmente non richiede la libreria nativa HTTP) sia
// WS sullo stesso server
var serverHTTP = http.createServer()

// Memorizzo la porta del server
const PORTA_SERVER = 8080

// Inizializzo express
// Express, in questo caso, permette di evitare di scrivere tutta la
// Parte di gestione dei file perché se ne occupa lui
var app = express()

// Parso i cookie per evitare di doverli gestire manualmente
app.use(cookieParser())

// Parso il corpo della richiesta
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Configuro il server WS
var wss = new Ws({ server: serverHTTP })

// Controllo che l'utente abbia effettuato l'accesso se punta
// A pagine diverse da /server/accesso o /accesso.html o i file css o i file JS
app.use('/', require('./ajax/controlloAccesso'))

// Pagina per l'accesso (accetta solo connessioni di tipo POST)
app.post('/ajax/accesso', require('./ajax/accesso'))

// Pagina per la disconessione
app.use('/disconnessione', require('./ajax/disconnessione'))

// Utilizzo express per servire i file html
// I file sono statici dentro la cartella www
// Quindi utilizzo il metodo "static"
// http://expressjs.com/en/starter/static-files.html
app.use('/', express.static('www'))

// Richiesta di connessione in arrivo per il WSS
wss.on('connection', (sock) => {

  console.log('[WSS] Un utente si è connesso!')

  // Saluto l'utente tramite i WS
  sock.send('Ciao utente simpatico!')
  sock.send('Inviami dei messaggi per provare la connessione socket!')

  // Mi metto in ascolto per i messaggi che l'utente
  // Invia tramite i WS
  sock.on('message', (msg) => {
    console.log(`[WSS] L'utente ha inviato: ${msg}`)
    sock.send(`Hey utente simpatico, hai inviato: ${msg}`)
  })
})

// Invio le richieste a express
serverHTTP.on('request', app)

// Mi metto in ascolto
serverHTTP.listen(PORTA_SERVER, (err) => {

  // Errore
  if(err)
    console.log('Errore!')

  // Tutto okay
  else
    console.log('[HTTP] Il server è stato creato correttamente sulla porta', serverHTTP.address().port)

})
