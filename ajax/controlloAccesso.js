var fs = require('node-fs-extra')

module.exports = (req, res, next) => {

  // Leggo gli utenti dal file
  var utenti = fs.readJsonSync(__dirname+'/../utenti.json')

  // Se l'utente non richiede una delle pagine di accesso
  // Controllo che sia loggato
  if(req.path !== '/ajax/accesso' && req.path !== '/accesso.html' && req.path.indexOf('/media/') !== 0 && req.path.indexOf('/materialize/') !== 0 && req.path.indexOf('/js/') !== 0 && req.path.indexOf('/css/') !== 0) {

    // Il cookie non esiste o non è valido
    if(req.cookies.sessione === undefined || req.cookies.sessione.length !== 32) {
      res.redirect('/accesso.html')
      res.end()

    // Controllo che la sessione esista
    } else {

      var accesso = false

      for(var i = 0; i < utenti.length; i++) {

        if(utenti[i].sessioni !== undefined) {
          for(var j = 0; j < utenti[i].sessioni.length; j++) {

            if(utenti[i].sessioni[j].hash === req.cookies.sessione && utenti[i].sessioni[j].scadenza >= Date.now())
              accesso = true
          }
        }
      }

      if(accesso === true)
        next()

      else {
        res.redirect('/accesso.html')
        res.end()
      }
    }
  // L'utente non richiede una pagina di accesso
  } else
    next()
}