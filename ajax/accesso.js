// Importo le librerie
var fs = require('node-fs-extra')

// Creo una funzione md5
function md5(txt) {
  return require('crypto').createHash('md5').update(txt).digest('hex')
}

// Esporto il metodo di accesso
module.exports = (req, res) => {

  var utenti = fs.readJsonSync(__dirname+'/../utenti.json')

  // Memorizzo i dati di accesso
  var dati = req.body

  // Controllo i dati
  if(dati.user === '' || dati.user === undefined)
    res.end(JSON.stringify({ errore: true, msg: 'Devi inserire un nome utente!' }))

  else if(dati.pwd === '' || dati.pwd === undefined)
    res.end(JSON.stringify({ errore: true, msg: 'Devi inserire una password!' }));

  // Controllo le credenziali
  else {

    var accesso = false

    for(var i = 0; i < utenti.length; i++) {

      if(utenti[i].username === dati.user && utenti[i].pwd === md5(dati.pwd))
        accesso = i
    }

    // L'utente ha effettuato l'accesso
    if(accesso !== false) {

      // Se nel file non è presente la proprietà delle sessioni la creo
      // Altrimenti la copio
      if(utenti[accesso].sessioni !== undefined)
        var sessioni = utenti[accesso].sessioni

      else
        var sessioni = []

      // Genero un'hash della sessione
      var hashSessione = md5(Date.now().toString())

      // Scadenza della sessione
      var scadenza = 3600000; // Un'ora
      if(dati.prolungamento === true)
        scadenza = 604800000; // Una settimana

      // Salvo la sessione
      sessioni.push({ hash: hashSessione, scadenza: Date.now()+scadenza })
      utenti[accesso].sessioni = sessioni
      fs.writeJsonSync(__dirname+'/../utenti.json', utenti)

      // Imposto il cookie
      res.cookie('sessione', hashSessione, { expires: new Date(Date.now()+scadenza) })

      // Chiudo la richiesta
      res.end('{}');

    // L'utente non ha effettuato l'accesso
    } else
      res.end(JSON.stringify({ errore: true, msg: 'Le credenziali non sono valide!' }));

  }


  // Pulisco il file dalle sessioni scadute
  for(var i = 0; i < utenti.length; i++) {
    for(var j = 0; j < utenti[i].sessioni.length; j++) {
      if(utenti[i].sessioni[i].scadenza < Date.now()) {
        utenti[i].sessioni.splice(j, 1)
        j--
      }
    }
  }

  // Salvo su disco gli utenti e le sessioni
  fs.writeJsonSync(__dirname+'/../utenti.json', utenti)

}