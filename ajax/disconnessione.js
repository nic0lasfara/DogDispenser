// Esporto il metodo di accesso
module.exports = (req, res) => {

    // Creo un cookie vuoto con scadenza nel passato
    res.cookie('sessione', '', { expires: new Date(Date.now()-3600*24) })

    // Rendirizzo ad un'altra pagina
    res.redirect('/')
}
